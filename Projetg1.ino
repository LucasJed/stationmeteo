#include <SPI.h>
#include <WiFi101.h>
#include "DHT.h"

#define DHTPIN 2    // what pin we're connected to, pin1 is 5th pin from end

// Uncomment whatever DHT sensor type you're using!
//#define DHTTYPE DHT11  // DHT 11
//#define DHTTYPE DHT21  // DHT 21
#define DHTTYPE DHT22  // DHT 22

#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;





// ThingSpeak Settings
char server[] = "api.thingspeak.com";
String writeAPIKey = "44HQCRFIO5X8C75E";

bool Sendingcloud = false;

// initialisation of variable to be send
int humidityData;
int celData; 
int fehrData;
int hicData;
int hifData;
int lightData;
int airQData;
int mark=0;
bool unity = true; 
// true = celcius , false =  fehr


// initialisation of pins
const int lightsensor= A1;
const int airQsensor=A2;
const int buttonpin=0;
const int ledpin=1;



// initialisation of threshold value
int infhum2 = 37;
int infhum = 45;
int suphum = 55;
int suphum2 = 62;
int inftemp2 = 16;
int inftemp = 19;
int suptemp = 24;
int suptemp2 = 27 ; 
int airQ1 = 100;
int airQ2 = 200;
int airQ3 = 400;
int airQ4 = 700;
int influx = 800;

// initialisation of flags 
bool lightok;
bool humok;
bool tempok;
bool airQok;
String airQstring;

// initialisation of password
String password = "mdp";
String researchpassword;

DHT dht(DHTPIN,DHTTYPE);

// initialisation of internet connexion and server connexion
const char WEBSITE[] = "api.pushingbox.com"; //pushingbox API server
const String devid = "vE18FDB7B2168647"; //device ID on Pushingbox for our Scenario

const char* MY_SSID = "Morgot";
const char* MY_PWD =  "tsql5500";


int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)

// mesurement function
void recupdata(){
  humidityData = dht.readHumidity();
  // Read temperature as Celsius (the default)
  celData = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  fehrData = dht.readTemperature(true);
  lightData = analogRead(lightsensor);
  airQData = analogRead(airQsensor); 
  
}

// marking function
void setmark(){
  mark = 0 ;
  lightok = false;
  humok = false;
  tempok= false;
  airQok = false;
  if (lightData > influx){
    mark++;
    lightok= true;
  }
  if (humidityData > infhum2 && humidityData < suphum2){
    mark ++ ;
  }
  if (humidityData > infhum && humidityData < suphum){
    mark ++ ;
    humok=true;
  } 
  if (celData > inftemp2 && celData < suptemp2){
    mark ++ ;
  }
   if (celData > inftemp && celData < suptemp){
    mark ++ ;
    tempok = true;
  }
   if (airQData <airQ4){
    mark ++ ;
    airQstring = "Poor";
  }
   if (airQData <airQ3){
    mark ++ ;
    airQstring="Bad";
  }
   if (airQData <airQ2){
    mark = mark +2 ;
    airQok= true;
    airQstring="Good";
  }
   if (airQData <airQ1){
    mark ++ ;
    airQstring="Excellent";
  }
  Serial.print("mark : ");
  Serial.println(mark);
}

// sending data by button
void buttoninterupt (){
    Sendingcloud=!Sendingcloud;
   Serial.println("Envoi au cloud");
   

}

// lcd function
void ldcmanager(){
   lcd.clear();
   lcd.setRGB(25*(10-mark), 25*(mark),0 );
    
   // humidity 
   lcd.setCursor(0,0);
   lcd.print("Humidity: ");
   lcd.setCursor(0,1); 
   lcd.print(humidityData);
   lcd.print(" %");
   if (humok == false){
    digitalWrite(ledpin,HIGH);
   } else {
   digitalWrite(ledpin,LOW);
   }
   delay(3000);
   lcd.clear();

   // temperature
   lcd.setCursor(0,0);
   lcd.print("Temperature: ");
   lcd.setCursor(0,1); 
   if ( unity) {
    lcd.print(celData);
   lcd.print(" C");
   } else {
     lcd.print(fehrData);
   lcd.print(" F");
   }
   if (tempok == false){
    digitalWrite(ledpin,HIGH);
   } else {
   digitalWrite(ledpin,LOW);
   }
   delay(3000);
   lcd.clear();

    // luminosity
   lcd.setCursor(0,0);
   lcd.print("luminosity: ");
   lcd.setCursor(0,1); 
   lcd.print(lightData);
   lcd.print(" Lux");
   if (lightok == false){
    digitalWrite(ledpin,HIGH);
   } else {
   digitalWrite(ledpin,LOW);
   }
   delay(3000);
   lcd.clear();

  // air quality
   lcd.setCursor(0,0);
   lcd.print("Air Quality: ");
   lcd.setCursor(0,1); 
   lcd.print(airQstring);
   if (airQok == false){
    digitalWrite(ledpin,HIGH);
   } else {
   digitalWrite(ledpin,LOW);
   }
   delay(3000);
   lcd.clear();

   lcd.setCursor(0,0);
   lcd.print("Mark : ");
   lcd.setCursor(0,1); 
   lcd.print(mark);
   lcd.print("/10");
   delay(3000);
   lcd.clear();

   lcd.print("Sending Data");
  }


void setup() {
  
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  pinMode(ledpin,OUTPUT);
  pinMode(buttonpin,INPUT);
  researchpassword = "";
  lcd.begin(16,2);
  attachInterrupt(buttonpin,buttoninterupt,FALLING);
  delay(1000);
  Serial.println("Enter a password to continue");
  while (!researchpassword.equals((String) "mdp\n")){
    while(Serial.available()) {
      researchpassword = (String) Serial.readString();
      Serial.println(researchpassword);
    }
  }
  Serial.print("ok");
  while (!Serial) 
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) 
  {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(MY_SSID);
    //Connect to WPA/WPA2 network.Change this line if using open/WEP network
    status = WiFi.begin(MY_SSID, MY_PWD);

    // wait 10 seconds for connection:
    delay(10000);
  }
  
  Serial.println("Connected to wifi");
  printWifiStatus();
  
}

void loop() {

  recupdata();
  
  String led;
  // Check if any reads failed and exit early (to try again).
  if (isnan(humidityData) || isnan(celData) | isnan(fehrData) | isnan(lightData) | isnan(airQData))
  {
    Serial.println("Failed to read sensors!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  hifData = dht.computeHeatIndex(fehrData, humidityData);
  // Compute heat index in Celsius (isFahreheit = false)
  hicData = dht.computeHeatIndex(celData, humidityData, false);

  Serial.print("Humidity: ");
  Serial.print(humidityData);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(celData);
  Serial.print(" *C ");
  Serial.print(fehrData);
  Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hicData);
  Serial.print(" *C ");
  Serial.print(hifData);
  Serial.println(" *F\n");
  Serial.print("Light :");
  Serial.print(lightData);
  Serial.println("Lux");
  Serial.print("air quality :");
  Serial.println(airQData);

  
   char inchar = (char)Serial.read();
  if (inchar=='c'){
    unity = true;
  }
  if (inchar=='f'){
    unity = false;
  }

  setmark();
  ldcmanager();

 sendingdata();
  if (Sendingcloud){
    httpRequest();
    
  }
}

void sendingdata(){
  Serial.println("\nSending Data to Server..."); 
  // if you get a connection, report back via serial:
  WiFiClient client;  //Instantiate WiFi object, can scope from here or Globally
    //API service using WiFi Client through PushingBox then relayed to Google
    if (client.connect(WEBSITE, 80))
      { 
         client.print("GET /pushingbox?devid=" + devid
       + "&humidityData=" + (String) humidityData
       + "&celData="      + (String) celData
       + "&fehrData="     + (String) fehrData
       + "&hicData="      + (String) hicData
       + "&hifData="      + (String) hifData
       + "&lightData="      + (String) lightData
       + "&airQData="      + (String) airQData
       + "&unity="      + (String) unity
       + "&mark="      + (String) mark
         );

      // HTTP 1.1 provides a persistent connection, allowing batched requests
      // or pipelined to an output buffer
      client.println(" HTTP/1.1"); 
      client.print("Host: ");
      client.println(WEBSITE);
      client.println("User-Agent: MKR1000/1.0");
      //for MKR1000, unlike esp8266, do not close connection
      client.println();
      Serial.println("\nData Sent"); 
      client.stop();
      }

      delay(5000); //wait for client to close
      
}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void httpRequest() {
  WiFiClient client;  //Instantiate WiFi object, can scope from here or Globally
  Serial.println("Sending data to cloud");
  // create data string to send to ThingSpeak
  String data = String("field1=" + String(celData, DEC) + "&field2=" + String(humidityData, DEC)+ "&field3=" + String(airQData, DEC) + "&field4=" + String(lightData, DEC) + "&field5=" + String(hicData, DEC)+ "&field6=" + String(fehrData, DEC) + "&field7=" + String(hifData, DEC) ); 
  // close any connection before sending a new request
  client.stop();
  // POST data to ThingSpeak
  if (client.connect(server, 80)) {
    client.println("POST /update HTTP/1.1");
    client.println("Host: api.thingspeak.com");
    client.println("Connection: close");
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("X-THINGSPEAKAPIKEY: "+writeAPIKey);
    client.println("Content-Type: application/x-www-form-urlencoded");
    client.print("Content-Length: ");
    client.print(data.length());
    client.print("\n\n");
    client.print(data);
  }
   Serial.println("\nData Sent"); 
      client.stop();

      delay(5000); //wait for client to close
}
